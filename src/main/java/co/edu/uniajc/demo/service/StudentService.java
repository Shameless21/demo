package co.edu.uniajc.demo.service;


import co.edu.uniajc.demo.model.StudentModel;
import co.edu.uniajc.demo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {
    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }

    public StudentModel createStudent(StudentModel studentModel){
        return studentRepository.save(studentModel);
    }

    public StudentModel updateStudent(StudentModel studentModel){
        return studentRepository.save(studentModel);
    }

    public void deleteStudent(Long id){
        studentRepository.deleteById(id);
    }

    //public List<StudentModel> findAllStudent(){
      //  return StudentRepository.findAll();
    //}
}
